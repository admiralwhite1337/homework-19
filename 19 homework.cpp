﻿#include <iostream>
#include <string>
#include <stack>

using namespace std;

class Animal
{
public:
	virtual void Voice(int x)
	{
		cout << "Голос животного!" << endl;
	}
};

class Dog : public Animal
{
public:
	void Voice(int x) override
	{	
		if (x == 0)
		cout << "Гав!" << endl;
	}
};

class Cat : public Dog
{
public:
	void Voice(int x) override
	{
		if (x == 1)
		cout << "Мяу!" << endl;
	}
};

class Bird : public Cat
{
public:
	void Voice(int x) override
	{
		if (x == 2)
		cout << "Чик-чирик!" << endl;
	}
};

class Fox : public Bird
{
public:
	void Voice(int x) override
	{
		if (x == 3)
		cout << "Яяу!" << endl;
	}
};

int main()
{
	setlocale(LC_ALL, "Russian");

	Animal* v4 = new Fox;
	Animal* v3 = new Bird;
	Animal* v2 = new Cat;
	Animal* v1 = new Dog;
	int Row [4] = {1,2,3,4};
	for (int i = 0; i<4; i++)
	{
		cout << "\n" << "Животное №" << i + 1 << " говорит: ";
		v4->Voice(i);
		v3->Voice(i);
		v2->Voice(i);
		v1->Voice(i);
	}
}